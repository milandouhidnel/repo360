package edu.citbyui.worktime.model;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;


@Dao

public abstract class SessionDAO {
    @Query("SELECT COUNT(*) FROM Session")
    public abstract long count();

    @Query("SELECT * FROM Session")
    public abstract List<Session> getAll();

    @Query("SELECT * FROM Session WHERE 'key' = :key")
    public abstract Session getByKey(long key);

    public long insert(Session session) {
        long id = insertH(session);
        session.setKey(id);
        return id;
    }
    @Insert
    abstract long insertH(Session session);


    @Update
    public abstract void update(Session session);

    @Delete
    public abstract void delete(Session session);

    @Query("DELETE FROM Session")
    public abstract void deleteAll();
}
