package edu.citbyui.worktime.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import edu.citbyui.worktime.R;
import edu.citbyui.worktime.model.AppDatabase;
import edu.citbyui.worktime.model.ProjectDAO;
import edu.citbyui.worktime.model.Project;


final class ProjectAdapter
        extends RecyclerView.Adapter<ProjectAdapter.ViewHolder> {
    // A reference to the main activity
    private final MainActivity activity;

    // A reference to the project DAO to read and write to the Project table.
     private final ProjectDAO projDao;

    // A list of all projects stored in the Project table. This is an in
    // memory copy of the projects that are stored in non-volatile memory in
    // the Project table of the Room database.
      private final List<Project> dataset;


    ProjectAdapter(MainActivity activity) {
        // Tell the RecyclerView that the project keys are stable.
        setHasStableIds(true);

        this.activity = activity;

        // Todo: Get a reference to the project DAO.
        Context ctx = activity.getApplicationContext();
        AppDatabase db = AppDatabase.getInstance(ctx);
        projDao = db.getProjectDAO();


        // Todo: Get a list of all the projects in the Project table.
        dataset = projDao.getAll();


    }

    @Override
    public int getItemCount() {
        // Todo: Return the number of elements that
        // are stored in the list of projects.
        return dataset.size();
    }

    @Override
    public long getItemId(int pos) {
        // Todo: Return the key of the project that is
        // stored in the list of projects at index pos.
        return dataset.get(pos).getKey();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_project, parent, false);
        return new ViewHolder(view);
    }

    // This onBindViewHolder method will be called each time that the
    // WorkTime app displays a project in a row of the RecyclerView.
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int pos) {
        try {
            holder.bind(pos);
        }
        catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }
    }


    // Each ViewHolder object corresponds to one row in the RecyclerView.
    // Each ViewHolder object will hold three TextViews that display a project
    // to the user.
    final class ViewHolder extends RecyclerView.ViewHolder
            implements OnLongClickListener, OnMenuItemClickListener, View.OnClickListener {

        // References to the three TextViews in this row.
        private final TextView txtStart, txtTitle, txtDurat;

        // A reference to the project that is displayed in this ViewHolder.
        private Project project;

        ViewHolder(View view) {
            super(view);

            // Todo: Get a reference to each of the three TextViews.
            txtStart = view.findViewById(R.id.textView2);
            txtTitle = view.findViewById(R.id.textView3);
            txtDurat = view.findViewById(R.id.textView4);


            //view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        // Bind this ViewHolder to the project that is
        // stored at index pos in the list of all projects.
        void bind(int pos) {
            project = dataset.get(pos);

            // Todo: Display the data in project in the three TextViews.
            txtTitle.setText(project.getName());
        }

        // This onLongClick method will be called when the
        // user long presses one row in the RecyclerView.
        @Override
        public boolean onLongClick(View view) {
            try {
                // Create a popup menu and show it to the user.
                PopupMenu menu = new PopupMenu(activity, view);
                menu.getMenuInflater().inflate(R.menu.popup, menu.getMenu());
                menu.setOnMenuItemClickListener(this);
                menu.show();
            } catch (Exception ex) {
                Log.e(MainActivity.TAG, ex.toString());
            }
            return true;
        }

        // This onMenuItemClick method will be called when
        // the user presses an item on the popup menu.
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            boolean handled = false;
            try {
                switch (item.getItemId()) {
                    case R.id.itmDelete:
                        // Todo: Delete from the Project table and
                        projDao.delete(project);
                        int index = dataset.indexOf(project);
                        dataset.remove(index);

                        // remove from the in memory list of projects
                        // the project that the user long pressed.

                        notifyItemRemoved(index);
                        handled = true;
                        break;
                }
            } catch (Exception ex) {
                Log.e(MainActivity.TAG, ex.toString());
            }
            return handled;
        }

        @Override
        public void onClick(View v) {

        }

    }
}
