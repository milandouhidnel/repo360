package edu.citbyui.worktime.view;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import edu.citbyui.worktime.R;
import edu.citbyui.worktime.model.AppDatabase;
import edu.citbyui.worktime.model.Project;
import edu.citbyui.worktime.model.ProjectDAO;

public class AddProjectFrag extends Fragment {
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstState) {
        View view = null;
        try {
            super.onCreateView(inflater, container, savedInstState);
            view = inflater.inflate(R.layout.frag_add_project, container, false);

            Button btnCancel = view.findViewById(R.id.button3);
            btnCancel.setOnClickListener(new HandleCancel());

            Button btnAdd = view.findViewById(R.id.button2);
            btnAdd.setOnClickListener(new HandleAdd());


            //Get the floating action button and add a function to it
            //  FloatingActionButton fab = view.findViewById(R.id.floatingActionButton);
            //fab.setOnClickListener(new MainFrag.HandleAddProjectClick());
        } catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }
        return view;
    }

    private final class HandleCancel implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Activity act = getActivity();
            act.onBackPressed();
        }


        }
    private final class HandleAdd implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            AppCompatActivity act = (AppCompatActivity) getActivity();

            EditText editTextName = view.findViewById(R.id.editText);
            String name = editTextName.getText().toString();
            EditText editTextDescription = view.findViewById(R.id.editText3);
            String description = editTextDescription.getText().toString();

            Context ctx = act.getApplicationContext();
            AppDatabase db = AppDatabase.getInstance(ctx);
            ProjectDAO projectDAO = db.getProjectDAO();

            Project project = new Project(name, description);
            projectDAO.insert(project);

            act.onBackPressed();
        }
    }
}
