package edu.citbyui.worktime.model;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;
@Dao

public abstract class ProjectDAO {
    @Query("SELECT COUNT(*) FROM Project")
    public abstract long count();

    @Query("SELECT * FROM Project")
    public abstract List<Project> getAll();

    //@Query("SELECT COUNT(*) FROM Project")
    //public abstract long getByKey();
    @Query("SELECT * FROM Project WHERE 'key' = :key")
    public abstract Project getByKey(long key);

    public long insert(Project project) {
        long id = insertH(project);
        project.setKey(id);
        return id;
    }
    @Insert
    abstract long insertH(Project project);



    @Update
    public abstract void update(Project project);

    @Delete
    public abstract void delete(Project project);

    @Query("DELETE FROM Project")
    public abstract void deleteAll();
}
