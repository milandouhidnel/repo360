package edu.citbyui.worktime.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Project {
    @PrimaryKey(autoGenerate = true)
    private long key;
    //@ForeignKey(entity = Project.class, childColumns = "key",parentColumns = "projeckKey")
    private String name;
    private String description;

    Project() {
    }


    @Ignore
    public Project(long key) {
        this.key = key;
    }

    @Ignore
    public Project(String name, String description) {
        this.name = name;
        this.description = description;

    }

    public long getKey() {
        return key;
    }

    void setKey(long key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        boolean eq = false;
        if (obj instanceof Project) {
            Project other = (Project)obj;
            eq = this.key == other.key &&
                    this.description.equals(other.description) &&
                    this.name.equals(other.name);
        }
        return eq;
    }


    @Override
    public String toString() {
        return "Project{" +
                "key=" + key +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
