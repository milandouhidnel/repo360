package edu.citbyui.worktime.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import edu.citbyui.worktime.R;

public class MainFrag extends Fragment {
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstState) {
        View view = null;

        try {
            super.onCreateView(inflater, container, savedInstState);
            view = inflater.inflate(R.layout.frag_main, container, false);

            //Get the floating action button and add a function to it
            FloatingActionButton fab = view.findViewById(R.id.floatingActionButton);
            fab.setOnClickListener(new HandleAddProjectClick());
        }
        catch (Exception ex) {
            Log.e(MainActivity.TAG, ex.toString());
        }
        return view;
    }

    private final class HandleAddProjectClick implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            //Activity act = getActivity();
          //Toast.makeText(act, "fab was clicked", Toast.LENGTH_SHORT).show();
            AddProjectFrag fragAdd = new AddProjectFrag();
            AppCompatActivity act = (AppCompatActivity)getActivity();
            FragmentTransaction trans =
                    act.getSupportFragmentManager().beginTransaction();
            trans.replace(R.id.fragContainer, fragAdd);
            trans.addToBackStack(null);
            trans.commit();
        }
    }

}
