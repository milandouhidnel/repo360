package edu.citbyui.worktime.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;

import java.util.Date;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(foreignKeys = { @ForeignKey(entity = Project.class,
        parentColumns = "key",
        childColumns = "projectKey", onDelete = CASCADE)
})
public class Session {
    @PrimaryKey(autoGenerate = true)
    private long key;
    //@ForeignKey(entity = Project.class,parentColumns = "key", childColumns = "projectKey")

    private long projectKey;

    private String description;

    @TypeConverters(Converters.class)
    private Date start;
    @TypeConverters(Converters.class)
    private Date end;


    Session() {}

    @Ignore
    public Session (long key){
        this.key = key;
    }

    @Ignore
    public Session (long projectKey, String description, Date start, Date end){
        this.projectKey = projectKey;
        this.start = start;
        this.description = description;
        this.end = end;
    }

    public long getKey() {
        return key;
    }
    void setKey (long key){
        this.key = key;
    }
    public long getProjectKey(){
        return projectKey;
    }
    void setProjectKey (long projectKey){
        this.projectKey = projectKey;
    }
    public String getDescription(){
        return description;
    }
    public void setDescription (String description){
        this.description = description;
        //return description;
    }
    public void setStart (Date Start){
        this.start = start;
    }
    public Date getStart(){
        return start;
    }
    public void setEnd(Date end){
        this.end = end;
    }
    public Date getEnd(){
        return end;
    }
    @Override
    public boolean equals( Object obj) {
        boolean eq = false;
        if (obj instanceof Session) {
            Session other = (Session)obj;
            eq = this.key == other.key &&
                    this.start.equals(other.start) &&
                    this.end.equals(other.end);
        }
        return eq;
    }


    @Override
    public String toString() {
        return "Session{" +
                "key=" + key +
                ", projectKey=" + projectKey +
                ", description='" + description + '\'' +
                ", start=" + start +
                ", end=" + end +
                '}';
    }
}
