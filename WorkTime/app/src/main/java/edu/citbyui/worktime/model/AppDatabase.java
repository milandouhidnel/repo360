package edu.citbyui.worktime.model;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;


@Database(entities = { Project.class, Session.class }, version = 2, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase singleton;

    public static AppDatabase getInstance(Context appCtx) {
        if (singleton == null) {
            singleton = Room.databaseBuilder(
                    appCtx, AppDatabase.class, "Record")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return singleton;
    }

    public abstract ProjectDAO getProjectDAO();
    public abstract SessionDAO getSessionDAO();
}
