package edu.citbyui.worktime.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import edu.citbyui.worktime.R;


public class MainActivity extends AppCompatActivity {
	public static final String TAG = "WorkTime";

	@Override
	protected void onCreate(Bundle savedInstState) {
		try {
			super.onCreate(savedInstState);
			setContentView(R.layout.activity_main);

			if (savedInstState == null) {
				// Create the main fragment and place it
				// as the first fragment in this activity.
				Fragment frag = new MainFrag();
				FragmentTransaction trans =
						getSupportFragmentManager().beginTransaction();
				trans.add(R.id.fragContainer, frag);
				trans.commit();
			}

		}
		catch (Exception ex) {
			Log.e(TAG, ex.toString());
		}
	}



	/*
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	//	TextView console = findViewById(R.id.console);
	//	System.setOut(new PrintStream(new TextViewWriter(console)));
	}

	private static final class TextViewWriter extends OutputStream {
		private final StringBuilder buffer;
		private final TextView console;

		TextViewWriter(TextView console) {
			this.buffer = new StringBuilder();
			this.console = console;
		}

		@Override
		public void write(int b) {
			buffer.append(b);
			console.setText(buffer);
		}

		@Override
		public void write(byte[] b, int offs, int len) {
			buffer.append(new String(b, offs, len));
			console.setText(buffer);
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		main();
	}


	private void main() {
		/* Write your code in this function as if this function were
		 * public static void main(String[] args)
		 */

	/*
		Context appCtx = getApplicationContext();
		AppDatabase db = AppDatabase.getInstance(appCtx);
		ProjectDAO projDAO = db.getProjectDAO();
		SessionDAO sessionDAO = db.getSessionDAO();


		Date date = new Date();
		Project first = new Project("CIT 360","I like this class");
		Project second = new Project("hello","this world");
		projDAO.insert(first);
		projDAO.insert(second);

	     Session project1 = new Session(first.getKey(),"this is my session",date , date);
	     Session project2 = new Session(second.getKey(),"Let's run the test", date, date);
	     Session session3 = new Session(first.getKey(),"I like this class",date, date);
	     sessionDAO.insert(project1);
	     sessionDAO.insert(project2);
	     sessionDAO.insert(session3);

		System.out.println(first.toString() + "\n" + second.toString());
		System.out.println(project1.toString() + "\n" + project2.toString() + "\n" + session3.toString());

	}

	*/
}
