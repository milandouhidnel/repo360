package edu.citbyui.worktime;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import org.junit.Test;


import java.util.Date;

import edu.citbyui.worktime.model.Project;
import edu.citbyui.worktime.model.ProjectDAO;
import edu.citbyui.worktime.model.Session;
import edu.citbyui.worktime.model.AppDatabase;
import edu.citbyui.worktime.model.SessionDAO;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public final class TestRoomDatabase {
    // Get the database and the data access objects.
    private Context ctx = InstrumentationRegistry.getTargetContext();
    private AppDatabase db = AppDatabase.getInstance(ctx);
    private SessionDAO sessionDAO = db.getSessionDAO();
    private ProjectDAO projDAO = db.getProjectDAO();


    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("edu.citbyui.worktime", appContext.getPackageName());
    }


    @Test
    public void testSessionTable() {
        // Delete all creatures and verify that
        // there are no rows in the Creature table.
       sessionDAO.deleteAll();
        projDAO.deleteAll();
        assertEquals(0, sessionDAO.count());

       Project project1 = new Project("mowgli", "Human");
       projDAO.insert(project1);



        Date date = new Date();
        Session first = new Session(project1.getKey(), "this is my session", date, date);
        Session second = new Session(project1.getKey(), "this is my session", date, date);
        sessionDAO.insert(first);
        sessionDAO.insert(second);
        assertEquals(2, sessionDAO.count());


        Session savedFirst = sessionDAO.getByKey(first.getKey());
        assertEquals(first, savedFirst);


        Session savedSecond = sessionDAO.getByKey(second.getKey());
        assertEquals(second, savedSecond);

        first.setDescription("newDescription");
        sessionDAO.update(first);
        assertEquals(2, sessionDAO);
        assertEquals(first, sessionDAO.getByKey(first.getKey()));
        assertEquals(second, sessionDAO.getByKey(second.getKey()));

        sessionDAO.delete(first);
        assertEquals(1, sessionDAO.count());

        assertEquals(second, sessionDAO.getByKey(second.getKey()));

        sessionDAO.deleteAll();
        assertEquals(0, sessionDAO.count());


        /*

        assertEquals(first, sessionDAO.getByKey(first.getKey()));
        assertEquals(second, sessionDAO.getByKey(second.getKey()));
        //update
        first.setDescription("This has to work!");
        assertEquals("This has to work!", first.getDescription());
        sessionDAO.update(first);
        assertEquals(first, sessionDAO.getByKey(first.getKey()));
        assertEquals(2, sessionDAO.count());
        assertEquals(first, sessionDAO.getByKey(first.getKey()));

        //test second
        assertEquals(second, sessionDAO.getByKey(second.getKey()));
        //delete first and check second
        sessionDAO.delete(first);
        assertEquals(1, sessionDAO.count());
        assertEquals(second, sessionDAO.getByKey(second.getKey()));
        // delete final
        sessionDAO.delete(second);
        */


    }


        @Test
        public void testProjectTable() {
            // Delete all creatures and verify that
            // there are no rows in the Creature table.
            sessionDAO.deleteAll();
            projDAO.deleteAll();
            assertEquals(0, projDAO.count());
            //Date date = new Date();

            Project project1 = new Project("CIT 360", "I like this class");
            Project project2 = new Project("CIT 360", "I like this class so much");

            projDAO.insert(project1);
            projDAO.insert(project2);

            assertEquals(2, projDAO.count());
            //assertEquals(first, projDAO.getByKey(first.getKey()));
            assertEquals(project1, projDAO.getByKey(project1.getKey()));
            //assertEquals(project2, projDAO.getByKey(project2.getKey()));
            assertEquals(project1, projDAO.getByKey(project1.getKey()));

            //update
            project1.setDescription("This has to work!");
            projDAO.update(project1);
            assertEquals(2, projDAO.count());
            assertEquals(project1, projDAO.getByKey(project1.getKey()));
            assertEquals(project2, projDAO.getByKey(project2.getKey()));

            //delete first and check second
            projDAO.delete(project2);
            assertEquals(1, projDAO.count());

            assertEquals(project1, projDAO.getByKey(project1.getKey()));
            // delete fina
            projDAO.deleteAll();
            assertEquals(0, projDAO.count());

        }
    }

